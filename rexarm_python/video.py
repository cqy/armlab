import cv2
import numpy as np
from PyQt4 import QtGui, QtCore, Qt

class Video():
    def __init__(self,capture):
        self.capture = capture
        self.capture.set(3, 1280)
        self.capture.set(4, 960)
        self.currentFrame=np.array([])
        self.w = int(self.capture.get(3))
        self.h = int(self.capture.get(4))
        """ 
        Affine Calibration Variables 
        Currently only takes three points: center of arm and two adjacent 
        corners of the base board
        Note that OpenCV requires float32 arrays
        """
        self.aff_npoints = 5                                     # Change!
        self.real_coordopcv = np.float32([[303.0, -297], [-302.,-297.], [-302.,310.]])
        self.mouse_coordopcv = np.float32([[0.0, 0.0],[0.0, 0.0],[0.0, 0.0]])      
        self.real_coord = np.float32([[303.0, -297], [-302.,-297.], [-302.,310.],[303.0,310.],[0.0,-207.0]])
        self.mouse_coord = np.float32([[0.0, 0.0],[0.0, 0.0],[0.0, 0.0],[0.0, 0.0],[0.0, 0.0]])      
        self.mouse_click_id = 0;
        self.aff_flag = 0;
        self.aff_matrix = np.float32((2,3))
        self.camera_matrix = np.loadtxt('cameraMatrix.cfg')
        self.dist_coefs = np.loadtxt('dist_coeff.cfg')
    def captureNextFrame(self):
        """                      
        Capture frame, convert to RGB, and return opencv image      
        """
        ret, frame=self.capture.read()
        if(ret==True):
            self.currentFrame=cv2.cvtColor(frame, cv2.COLOR_BAYER_GB2BGR)

    def convertFrame(self):
        """ Converts frame to format suitable for QtGui  """
        try:
            height,width=self.currentFrame.shape[:2]
            img=QtGui.QImage(self.currentFrame,
                              width,
                              height,
                              QtGui.QImage.Format_RGB888)
            img=QtGui.QPixmap.fromImage(img)
            self.previousFrame = self.currentFrame
            return img
        except:
            return None

    def loadCalibration(self):
        new_camera_matrix, roi = cv2.getOptimalNewCameraMatrix(self.camera_matrix,self.dist_coefs,(self.w,self.h),1,(self.w,self.h))
        self.currentFrame = cv2.undistort(self.currentFrame, self.camera_matrix, self.dist_coefs, None, new_camera_matrix)
        """
        Load calibration from file and applies to the image
        Look at camera_cal.py final lines to see how that is done
        """
        pass

