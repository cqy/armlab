import numpy as np
from numpy.linalg import inv
class Affine():
    def __init__(self,nPoint,wcoord,pcoord):
        self.nPoint =  nPoint
        self.wcoord = wcoord
        self.pcoord = pcoord
        self.A = np.zeros((2*self.nPoint,6))
        self.b = np.zeros((2*self.nPoint,1))
        self.affineMatrix = np.zeros((3,3))
        for index in range(0,self.nPoint):
            self.A[2 * index][0] = self.pcoord[index][0]
            self.A[2 * index][1] = self.pcoord[index][1]
            self.A[2 * index][2] = 1
            self.A[2 * index+1][3] = self.pcoord[index][0]
            self.A[2 * index+1][4] = self.pcoord[index][1]
            self.A[2 * index+1][5] = 1
            self.b[2 * index] = self.wcoord[index][0]
            self.b[2 *index+1] = self.wcoord[index][1]
    def calAffineMatrix(self):
        a =  inv(np.dot(np.transpose(self.A),self.A)) #inv[(A^T)A]]
        b = np.dot(a,np.transpose(self.A)) #(A^T)b 
        ans = np.dot(b,self.b) # x = inv[(A^T)A](A^T)b 
        print ans #[a;b;c;d;e;f]
        tmp = np.zeros((3,3))
        i = 0
        j = 0
        for x in ans:
            tmp[i][j] = x
            if j == 2:
                j = 0
                i = i+1
            else:
                j = j + 1
        tmp[2][2] = 1
        self.affineMatrix = tmp
        return tmp
       
'''nPoint = 6
wcoord = [[1,2],[2,8],[3,12],[14,5],[5,16],[6,17]]  
pcoord = [[-1,2],[-2,8],[-3,12],[-14,5],[-5,16],[-6,17]]    

a = Affine(nPoint,wcoord,pcoord)
print a.A
print a.b  
print a.calAffineMatrix()'''
